-- RHud created by Reeceb1, Optimised by Scalist.net

-- Loading HUD starting message
print("(HUD) The HUD has started loading!")

-- Font(s)
surface.CreateFont("mainFont", {
    font = "Arial",
    extended = false,
    size = 20,
    weight = 800,
    blursize = 0.5,
    scanlines = 0,
    antialias = true,
    underline = false,
    italic = true,
    strikeout = false,
    symbol = false,
    shadow = false,
    additive = false,
    outline = true,
})

-- Loading HUD's font message
print("(HUD) The HUD font have finished loadings")

-- Disable default HUD & red overlay on death
hook.Add("HUDShouldDraw", "clh_hidehud_default", function(name)
    if (name == "CHudHealth" or name == "CHudBattery" or name == "CHudAmmo" or name == "CHudCrosshair" or name == "CHudDamageIndicator") then
        return false
    end
end)

-- HUD Design
hook.Add("HUDPaint", "DrawMyHud", function() -- HUD hook

    -- Background Boxes
    draw.RoundedBox(10, 120, ScrH() - 203, 200, 100, Color(40, 40, 40, 200))
    draw.RoundedBox(10, 130, ScrH() - 193, 125, 45, Color(30, 33, 33, 200))
    draw.RoundedBox(10, 130, ScrH() - 145, 125, 34, Color(30, 33, 33, 200))
    draw.RoundedBox(10, 125, ScrH() - 235, 125, 30, Color(30, 33, 33, 200))
    draw.RoundedBox(5, 18, ScrH() - 198, 94, 94, Color(255, 0, 0)) -- avatar box

    -- Health Bar
    local health = LocalPlayer():Health()

    draw.RoundedBox(5, 25, ScrH() - 90, 356, 35, Color(40, 40, 40, 200))
    draw.RoundedBox(5, 28, ScrH() - 85.350, health * 3.5, 25, Color(255, 0, 0, 255))
    draw.SimpleText(health .. "%", "mainFont", 50 + 150, ScrH() - 72, Color(242, 242, 242, 255), 1, 1)

    -- Armor Bar
    local armor = LocalPlayer():Armor()

    draw.RoundedBox(5, 24.5, ScrH() - 50, 356, 34, Color(40, 40, 40, 200))
    draw.RoundedBox(5, 27, ScrH() - 45, armor * 3.5, 25, Color(32, 67, 226, 255))
    draw.SimpleText(armor .. "%", "mainFont", 200, ScrH() - 32, Color(242, 242, 242, 255), 1, 1)

    -- Money Text
    draw.SimpleText("Wallet: $" .. LocalPlayer():getDarkRPVar("money"), "mainFont", 187, ScrH() - 219, Color(242, 242, 242, 255), 1, 1)

    -- Wanted Text
    if LocalPlayer():isWanted() then
        draw.SimpleText("Wanted", "mainFont", 165, ScrH() - 137, Color(math.sin(CurTime() * 10) * 255, 0, 0), TEXT_ALIGN_CENTER)
    else
        draw.SimpleText("Not Wanted", "mainFont", 190, ScrH() - 137, Color(255, 255, 255, 255), TEXT_ALIGN_CENTER)
    end

    -- Gun license Text
    if LocalPlayer():getDarkRPVar("HasGunlicense") then
        draw.SimpleText(HasGunlicense .. "Licence", "mainFont", 195, ScrH() - 170, Color(242, 242, 242, 255), 1, 1)
    else
        draw.SimpleText("No License", "mainFont", 190, ScrH() - 170, Color(242, 242, 242, 255), 1, 1)
    end
end)

-- Avatar VGUI (do not put this in main hook as it will cause a memory leak)
hook.Add("InitPostEntity", "LocalPlayerCreate", function() -- Make a hook that scans for when LocalPlayer() is active
    hook.Remove("InitPostEntity", "LocalPlayerCreate") -- Remove hook as we do not need it

    -- Display avatar function (here LocalPlayer() return is valid player)
    local Avatar = vgui.Create("AvatarImage", Panel)
    Avatar:SetSize(90, 90)
    Avatar:SetPos(20, ScrH() - 196)
    Avatar:SetPlayer(LocalPlayer(), 64)
end)

-- Finished loading HUD message
print("(HUD) The HUD has finished loading!")