# RHud-Revamp
Welcome to the RHUD-Revamp repository! This contains 2 optimised, leak free version of a new HUD collection found here: https://forum.facepunch.com/search/user/rrqs/?type=Thread

This revamped version includes:
- New Avatar system that prevents memory leaks & auto-updates
- Clearer variable & function names
- Complete re-formatting of code to make it easier to read
- Updated methods of displaying the HUD
- Comments to help out fellow developers

## Installation (clientside)
If you are just using this HUD on a non-dedicated DarkRP server, follow these steps
1. Clone/Download this repository onto your PC
2. Open up the Garry's Mod directory, on Windows it is: ```C:\Program Files (x86)\Steam\steamapps\common\GarrysMod\garrysmod\lua\autorun\client```
3. Copy the ```cl_rhud_revamp.lua``` file into the directory listed above

## Installation (serverside)
If you are a server host on Garry's Mod & would like to use this HUD in your sever, follow these steps
1. Initalize a dedicated server running the DarkRP gamemode & the DarkRP edit addon
2. Clone/Download this repository onto your Server. For a Linux server, use: ```sudo apt-get install git``` ```mkdir ~/rhud-revamp``` ```cd ~/rhud-revamp``` ```sudo git clone https://github.com/JilkOwez/RHud-Revamp.git```
3. Head to your created server directory & copy the address
4. In Linux, use the ```mv``` command to move the ```cl_rhud_revamp.lua``` or ```cl_rhudbar_revamp.lua``` to the installation directory.

## Notes & Disclaimers
- This repository with the new source code was made with the explicit permission of the original creator.
- Feel free to share this around. If you do, please remember to credit the creator(s) of this product, in the source code or somewhere in-game.
- The installation guide for this repository is not complete, just a brief explanation on how to get you 90% of the way there.
