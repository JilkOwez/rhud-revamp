-- RHudBar created by Reeceb1, Optimised by Scalist.net

-- create font
surface.CreateFont("mainFont", {
    font = "Arial", -- Use the font-name which is shown to you by your operating system Font Viewer, not the file name
    extended = false,
    size = 35,
    weight = 800,
    blursize = 0,
    scanlines = 0,
    antialias = true,
    underline = false,
    italic = false,
    strikeout = false,
    symbol = false,
    shadow = false,
    additive = false,
    outline = true,
})

-- disable default HUD & red overlay on death
hook.Add( "HUDShouldDraw", "clh_hidehud_default", function(name)
    if (name == "CHudHealth" or name == "CHudBattery" or name == "CHudAmmo" or name == "CHudCrosshair" or name == "CHudDamageIndicator") then
        return false
    end
end)

-- HUD design
hook.Add("HUDPaint", "DrawMyHud", function()
    draw.RoundedBox(5, -3, ScrH() - 1050, 1700, 50, Color(40, 40, 40, 250))
    draw.RoundedBox(10, 7, ScrH() - 1042.9, 307, 35, Color(0, 0, 0, 120))
    draw.RoundedBox(10, 320, ScrH() - 1042, 307, 35, Color(0, 0, 0, 120))
    draw.RoundedBox(5, 633, ScrH() - 1039, 350, 29, Color(74, 209, 51, 255))
    draw.RoundedBox(5, 995, ScrH() - 1039, 300, 29, Color(167, 49, 211, 255))

    local health = LocalPlayer():Health()
    draw.RoundedBox(5, 10, ScrH() - 1039.9, health * 3, 30, Color(255, 120, 120))
    draw.SimpleText(health .. "%", "mainFont", 10 + 150, 26, Color(255, 255, 255, 255), 1, 1)

    local armor = LocalPlayer():Armor()
    draw.RoundedBox(5, 323.4, ScrH() - 1040, armor * 3, 30, Color(27, 77, 186, 250))
    draw.SimpleText(armor .. "%", "mainFont", 10 + 460, 26, Color(255, 255, 255, 255), 1, 1)

    draw.SimpleText("Wallet: $" .. LocalPlayer():getDarkRPVar("money"), "mainFont", 400 + 735, 26, Color(242, 242, 242, 255), 1, 1)
    draw.SimpleText("Name: " .. LocalPlayer():getDarkRPVar("rpname"), "mainFont", -25 + 840, 26, Color(242, 242, 242, 255), 1, 1)

    -- Display ammo
    if (LocalPlayer():GetActiveWeapon():IsValid()) then -- if player is alive then
        if (LocalPlayer():GetActiveWeapon():Clip1() != -1) then -- find & display client's current weapon ammo
            draw.SimpleText("Ammo: " .. client:GetActiveWeapon():Clip1() .. "/" .. client:GetAmmoCount(client:GetActiveWeapon():GetPrimaryAmmoType()), "2", 1412, ScrH() - 1042, Color(255, 255, 255, 255), 0, 0)
        end
    end
end)
